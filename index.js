const Web3 = require('web3')
var greeter = require('./greeter.json')
exports.handler = function(event, context, callback) {
        //var web3= new Web3('http://107.23.87.210:8545');
        var provider = new Web3.providers.HttpProvider('http://107.23.87.210:8545')
        web3 = new Web3(provider)
        //web3.eth.getAccounts().then(console.log);

        const contract = require('truffle-contract')
        const greeter_contract = contract(greeter)
        greeter_contract.setProvider(web3.currentProvider)

        var result = []
        web3.eth.getAccounts((error, accounts) => {
		for (var i=0, len=accounts.length; i<len; i++) {
			//var b = web3.eth.getBalance(accounts[i])
			result.push({ id : accounts[i], balance : 100, location : "Colony 2", name: "Szabolch", score: 227 })
		}
		console.log(result)
		callback(null, result)	

        });

  // or


};
